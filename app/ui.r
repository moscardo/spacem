#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Application title
  titlePanel(fluidRow(
    column(9, "SpaceM Viewer"), 
    column(3, img(height = 50, width = 50, src = "logo.png"))
    )),
  tags$p("Contact:", tags$a("Sergio Triana", href = "mailto:triana@embl.de"), ", EMBL Heidelberg"),
  fluidRow(
    column(width=8,selectizeInput("Sample", "Sample to Use", choices = c("Manuscript","Hepatocytes","Macrophages Low Range")),options = list(placeholder = 'select a Sample '),selected="Manuscript")
    #column(width=4,align='left',tags$img(src = "logo.png"))
  ),
  tabsetPanel(type = "tabs",
              tabPanel("Visualisation", 
                       fluidRow(width = 9, 
                                textOutput("dataset")
                       ),
                       fluidRow(
                         column(width=10, radioButtons("umap.type", label="What UMAP to use", selected = "Integrated UMAP", choices = c("SpaceM-UMAP","Integrated UMAP","Non-Integrated UMAP","Features UMAP","MOFA-UMAP"),inline=T))
                        ),
                       fluidRow(
                         column(width=5, radioButtons("tsne.type", label="What to highlight on the UMAP", selected = "Clusters", choices = c("Clusters","Datasets","Metabolites/Features"))),
                         column(width=5,uiOutput('columns'))
                       ),
                       fluidRow(
                         column(width= 5, tags$strong("UMAP"), tags$br(),
                                tags$i("This UMAP was calculated using the metabolites"),verbatimTextOutput("hover_info")),
                         column(width= 4, tags$strong("Cell Distribution"), tags$br(),
                                tags$i("This plot show the distribution of cells  by the selected variable")),
                         column(width=3,uiOutput('order'))
                       ),
                       fluidRow(
                         tags$head(tags$script('
                                               var dimension = [0, 0];
                                               $(document).on("shiny:connected", function(e) {
                                               dimension[0] = window.innerWidth;
                                               dimension[1] = window.innerHeight;
                                               Shiny.onInputChange("dimension", dimension);
                                               });
                                               $(window).resize(function(e) {
                                               dimension[0] = window.innerWidth;
                                               dimension[1] = window.innerHeight;
                                               Shiny.onInputChange("dimension", dimension);
                                               });
                                               '))
                         ,
                         #create rows / columns dynamically according to the number of plots
                         column(width=5,uiOutput('plotTsne'),style='padding:0px;',align="left"),
                         column(width=6,uiOutput('plotPie'),style='padding:0px;',align="left")
                         ),                       
                        fluidRow(column(offset = 3,width=6,align="center",uiOutput('plotPseudo'),style='padding:-100px;')),  
                         fluidRow(column(width=10,align="center",uiOutput('plotLegend'),style='padding:-100px;'))
                         )
              
              ,
              tabPanel("Differential expression",
                       fluidRow(
                         column(width=4,uiOutput('Cell1')),
                         column(width=4,uiOutput('Cell2')),
                         column(width=4, radioButtons("AssayDE", label="Assay to use", selected = "Metabolites", choices = c("Metabolites","Features")))),
                       fluidRow(
                         column(width=4,uiOutput('Cond1')),
                         column(width=4,uiOutput('Cond2')),
                         column(width=4,align="left",actionButton("calculate", "Calculate",style='padding:10px; font-size:100%'))
                                                                                                                                                                
                       ),
                       fluidRow(
                         column(width = 6, tags$strong("Volcano Plot")),
                         column(width= 4, tags$strong("Differential expression result"), tags$br())
                       ),
                       fluidRow(
                         #create rows / columns dynamically according to the number of plots
                         column(width=6,plotOutput("Voldano.pl",height="600px")),
                         column(width=4,withLoader(DTOutput("tableDE"),type='html',loader="dnaspin"))),
                       fluidRow(
                         column(width = 6, downloadButton("downloadData", "Download DE table")))
              )
             
              ))
)

