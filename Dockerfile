FROM rocker/shiny:3.6.1

RUN apt-get update && apt-get install -y libtiff-dev \
    libjpeg-dev libfftw3-dev libhdf5-dev libssl-dev \
    libxml2-dev libgmp3-dev     

ADD etc/ /etc/shiny-server/
COPY ./packages.R packages.R
ADD app/ /srv/shiny-server/


RUN Rscript packages.R

RUN sed -i '2ienv | grep AWS > /home/shiny/.Renviron && chown shiny.shiny /home/shiny/.Renviron\n' /usr/bin/shiny-server.sh

